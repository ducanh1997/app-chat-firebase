import moment from "moment";
import { Drawer } from "antd";
import { Button, Form, Input, DatePicker, Select } from "antd";
import { doc, getDoc, updateDoc } from "firebase/firestore";
import { db } from "../../firebase";
import { toast } from "react-toastify";
import { useDispatch } from "react-redux";
import { USER } from "../../redux/actions/const";

const { Option } = Select;

export const Profile = (props) => {
  const { data, onClose, visible } = props;
  const dispatch = useDispatch();

  const onFinish = (values) => {
    getDoc(doc(db, "users", data.uid)).then((docSnap) => {
      if (docSnap.exists()) {
        const userRef = doc(db, "users", data.uid);
        updateDoc(userRef, {
          dateOfBirth: values["dateOfBirth"].format("DD/MM/YYYY"),
          name: values.name,
          gender: values.gender,
        });
        toast("Update User Successfully!");
        onClose();
        const userUpdate = {
          ...userRef,
          dateOfBirth: values["dateOfBirth"].format("DD/MM/YYYY"),
          name: values.name,
          gender: values.gender,
        };
        dispatch({
          type: `${USER.USER_UPDATE}_SUCCESS`,
          payload: { user: userUpdate },
        });
      }
    });
  };

  return (
    <Drawer
      title="User profile"
      placement="left"
      onClose={onClose}
      visible={visible}
    >
      {data && (
        <Form
          name="normal_login"
          className="login-form mt-4"
          onFinish={onFinish}
          size="large"
          initialValues={{
            name: data?.name,
            dateOfBirth:
              data?.dateOfBirth !== ""
                ? moment(data?.dateOfBirth, "DD/MM/YYYY")
                : null,
            gender: data?.gender,
            email: data?.email,
          }}
        >
          <Form.Item
            name="name"
            hasFeedback
            rules={[{ required: true, message: "Please input your name!" }]}
          >
            <Input placeholder="Full name" />
          </Form.Item>
          <Form.Item
            hasFeedback
            name="dateOfBirth"
            rules={[{ required: true, message: "Please input your birthday!" }]}
          >
            <DatePicker
              format="DD/MM/YYYY"
              disabledDate={(current) => {
                return current && current > moment().startOf("day");
              }}
            />
          </Form.Item>
          <Form.Item
            hasFeedback
            name="gender"
            rules={[{ required: true, message: "Please select gender!" }]}
          >
            <Select placeholder="Select your gender">
              <Option value="male">Male</Option>
              <Option value="female">Female</Option>
            </Select>
          </Form.Item>
          <Form.Item hasFeedback name="email">
            <Input placeholder="Email" disabled />
          </Form.Item>
          <Form.Item>
            <Button
              type="primary"
              htmlType="submit"
              className="login-form-button w-100 text-uppercase"
            >
              Save
            </Button>
          </Form.Item>
        </Form>
      )}
    </Drawer>
  );
};
